/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author Pare
 */
public class Game {
    private Table table;
    private Player player1,player2;
    
    public Game(){
        player1 = new Player('X');
        player2 = new Player('O');    
    }
    
    public void play(){
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                showTable();
                showWin();
                showContinue();
                if(checkContinue()){
                    newTable();
                    newGame();
                    continue;
                }else{
                    showGameOver();
                    break;
                }
            }
            if(table.checkDraw()){
                showTable();
                showDraw();
                showContinue();
                if(checkContinue()){
                    newTable();
                    newGame();
                    continue;
                }else{
                    showGameOver();
                    break;
                }
            }
            table.switchPlayer();
        }
    }
    
    
    private void showWelcome(){
        System.out.println("Welcome To XO Game!");
    }  
    
    private void showTable(){
        char[][] t = table.getTable();
        for(int i =0;i<3;i++){
            for(int j =0;j<3;j++){
                System.out.print(t[i][j]+" ");
            }
            System.out.println();
        }
    }

    private void newGame() {
        table = new Table(player1,player2);
    }

    private void showTurn() {
        System.out.println(table.getCurrrentPlayer().getSymbol() + " Turn");
    }
    
    private void inputRowCol(){
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col: ");
            int row = kb.nextInt()-1;
            int col = kb.nextInt()-1;
            if (table.setRowCol(row, col)) {
                table.setRowCol(row, col);
                break;
            }
        }
    }

    private void showWin() {
        System.out.println(table.getCurrrentPlayer().getSymbol()+" Win");
    }

    private void showContinue() {
        System.out.print("Continue? [y/n]: ");
    }

    private void showDraw() {
        System.out.println("Darw");
    }

    private boolean checkContinue() {
        Scanner kb = new Scanner(System.in);
        String con = kb.next();
        if(con.equals("y")){
            return true;
        }else if(con.equals("n")){
            return false;
        }
        return false;
    }

    private void newTable() {
        char[][] t = table.resetTable();
    }

    private void showGameOver() {
        System.out.println("Game Over!!");
    }
}
