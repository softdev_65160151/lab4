/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author Pare
 */
public class Table {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player1;
    private Player player2;
    private Player currPlayer;
    private int turnCount = 0;
    private int row,col;
    
    public Table(Player player1 , Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currPlayer = player1;
    }
    
    public char[][] getTable(){
        return table;
    }
    
    public boolean setRowCol(int row, int col){
        if (table[row][col] != '-') {
            return false;
        }
        table[row][col] = currPlayer.getSymbol();
        this.row = row;
        this.col = col;
        return true;
    }
    
    public boolean checkWin(){
        if(checkRow()){
            return true;
        }if(checkCol()){
            return true;
        }if(checkX1()){
            return true;
        }if(checkX2()){
            return true;
        }
        return false;
    }
    
    private boolean checkRow(){
        for(int i=0; i<3; i++){
            if(table[row][i] != currPlayer.getSymbol()){
                return false;
            }
        }
        return true;
    } 
    
    private boolean checkCol(){
        for(int i=0;i<3;i++){
            if(table[i][col] != currPlayer.getSymbol()){
                return false;
            }
        }
        return true;
    } 
    
    private boolean checkX1(){
        for(int i=0;i<3;i++){
            if(table[i][i] != currPlayer.getSymbol()){
                return false;
            }
        }
        return true;
    } 
    
    private boolean checkX2(){
        if(table[0][2] != currPlayer.getSymbol() || table[1][1] != currPlayer.getSymbol() ||table[2][0] != currPlayer.getSymbol()){
            return false;
        }return true;
    } 

    Player getCurrrentPlayer() {
         return currPlayer;
    }

    public Player switchPlayer() {
        if (currPlayer == player1){
            return currPlayer = player2;
        } else {
            return currPlayer = player1;
        }
    }

    boolean checkDraw(){
        if(checkWin()== false){
            turnCount++;
            if(turnCount != 9){
                return false;
            }
        }
        return true;
    }

    char[][] resetTable() {
        for(int i=0;i<3;i++){
            for(int j=0; j<3;j++){
                table[i][j] = '-';
            }
        }
        return table;
    }
      
}